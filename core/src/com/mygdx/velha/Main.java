package com.mygdx.velha;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ScreenUtils;

public class Main extends ApplicationAdapter {
	SpriteBatch batch;
	Texture boardTexture;
	Texture xTexture;
	Texture circleTexture;
	private BitmapFont font;

	int[][] board = new int[3][3];
	final int CIRCLE = 2;
	final int X = 1;
	boolean isCircleTurn = false;
	boolean canPlay = true;

	Vector2[][] xPositions = {
		{
			new Vector2(70, 530),
			new Vector2(300, 530),
			new Vector2(530, 530)
		},
		{
			new Vector2(70, 300),
			new Vector2(300, 300),
			new Vector2(530, 300)
		},
		{
			new Vector2(70,70),
			new Vector2(300, 70),
			new Vector2(530, 70),
		}
	};
	Vector2[][] circlePositions = {
		{
			new Vector2(20, 490),
			new Vector2(260, 490),
			new Vector2(490, 490)
		},
		{
			new Vector2(20, 260),
			new Vector2(260, 260),
			new Vector2(490, 260)
		},
		{
			new Vector2(20, 20),
			new Vector2(260, 20),
			new Vector2(490, 20)
		},
	};
	
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		boardTexture = new Texture("bg.png");
		xTexture = new Texture("x.png");
		circleTexture = new Texture("circle.png");
		font = new BitmapFont();
		font.setColor(Color.RED);
	}

	@Override
	public void render () {
		ScreenUtils.clear(255, 255, 255, 1);
		if (Gdx.input.isTouched(Input.Buttons.LEFT)) {
			handleClick(Gdx.input.getX() / 260, Gdx.input.getY() / 260);
		}
		batch.begin();
		batch.draw(boardTexture, 33, 33);
		drawMarks(batch);
		if(!canPlay) {
			font.draw(batch, getWinnerText(), 300, 300);
		}
		batch.end();
	}

	@Override
	public void dispose () {
		batch.dispose();
		boardTexture.dispose();
		circleTexture.dispose();
		xTexture.dispose();
		font.dispose();
	}

	private void handleClick(int x, int y) {
		if(board[x][y] == 0 && canPlay) {
			int player = isCircleTurn ? CIRCLE : X;
			board[x][y] = player;
			boolean hasWon = checkWinnerMove(x, y, player);
			if (hasWon) {
				canPlay = false;
				System.out.println("WINNER!!");
				System.out.println(player);
				return;
			}
			isCircleTurn = !isCircleTurn;
		}
	}

	void drawMarks(SpriteBatch batch) {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				int cell = board[j][i];
				if (cell == X) {
					batch.draw(xTexture, xPositions[i][j].x, xPositions[i][j].y, 150, 150);
				}
				if (cell == CIRCLE) {
					batch.draw(circleTexture, circlePositions[i][j].x, circlePositions[i][j].y, 240, 240);
				}
			}
		}
	}

	String getWinnerText() {
		return isCircleTurn ? "O wins" : "X wins";
	}

	boolean checkWinnerMove(int x, int y, int player) {
		int col = 0;
		int row = 0;
		int diag = 0;
		int antiDiag = 0;
		for (int i = 0; i < 3; i++) {
			if (board[x][i] == player) col++;
			if (board[i][y] == player) row++;
			if (board[i][i] == player) diag++;
			if (board[i][1 - i + 1] == player) antiDiag++;
		}
		return col == 3 || row == 3 || diag == 3 || antiDiag == 3;
	}
}
